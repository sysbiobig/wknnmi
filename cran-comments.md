## Resubmission
This is a resubmission. In this version I have:

* Shortened the title, as requested.
* Removed the references to our paper in the documentation since it is not published yet (and not available as a preprint).
* Changed the role of Erica Tavazzi from "ctb" to "aut" as more appropriate.

## Resubmission
This is a resubmission. In this version I have:

* Removed the license file from the package, as requested.
* Removed the \emph{} markup from the Description, as requested.

## Test environments
* local Ubuntu Linux 16.04 install, R 3.5.2
* win-builder (devel and release)

## R CMD check results
There were no ERRORs or WARNINGs. 

There was 1 NOTE:

* checking CRAN incoming feasibility ... NOTE

  This is my first submission.
